User styles
===

How to install
---

Install the [chrome][stylish_chrome] or [firefox][stylish_firefox] extension.

After you'll need to help yourself a little, because I don't have time right now
to write a decent README

[stylish_chrome]: https://chrome.google.com/webstore/detail/stylish/fjnbnpbmkenffdnngjfgmeleoegfcffe
[stylish_firefox]: https://addons.mozilla.org/en-US/firefox/addon/stylish/?src=external-userstylesorghelp